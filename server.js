var io = require('socket.io')(6001);

io.on('connection', function(socket){
	
	console.log('connection', socket.id);
	socket.send('Send message');
	socket.emit('server-info', {version : .1});
	socket.broadcast.send({'message':'New user ' + socket.id});
	
	socket.join('vip', function(){
		console.log(socket.rooms);
	});
	
	socket.on('message', function(data){
		console.log(data);
		socket.broadcast.send(data);
	});
});